package com.me.mygdxgame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.me.mygdxgame.screen.LunarScreen;

/**
 * Classe servant à gérer les collision entre le module lunaire et le sol dans le cadre de l'utilisation du moteur physique.
 *
 */
public class CollisionListener implements ContactListener {

	private LunarScreen game;
	private SpriteBatch batch;

	public CollisionListener(LunarScreen gameScreen) {
		game = gameScreen;
		batch = new SpriteBatch();
	}

	/**
	 * Méthode déclenché lorsqu'un contact intervient dans le monde physique.
	 */
	@Override
	public void beginContact(Contact contact) {
		Fixture f1=contact.getFixtureA();
		Body b1=f1.getBody();
		Fixture f2=contact.getFixtureB();
		Body b2=f2.getBody();

		game.setTouchedGround(true);
		
		double vitesseAtterissage = -b2.getLinearVelocity().y;
		
		game.messageChoc = "Atterissage "+ crashState.getTypeAtterissage(vitesseAtterissage) +": " + game.affCorr(vitesseAtterissage, 2) + " m/s";		
	}

	@Override
	public void endContact(Contact contact) {
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}
}
