package com.me.mygdxgame;

import Jama.Matrix;

/**
 * Classe permettant d'effectuer les calculs lors de l'utilisation de commande en boucle ouverte
 *
 */
public class Commande {
	public static void main(String[] args) {
	}
	
	/**
	 * Calcul de la matrice G
	 * @param A
	 * @param B
	 * @return
	 */
	public Matrix calculGBoucleOuverte(Matrix A, Matrix B, int h){
		Matrix G = null;
		Matrix Apuiss = A;
		for(int n= 1; n<h-1; n++){
			// Calcul de la nouvelle valeur de la puissance de A
			Apuiss = Apuiss.times(Apuiss);
		}
		return G;
	}
	
	/**
	 * Calculer la valeur du vecteur U en fonction des matrice G et y
	 * U = G'(G*G')-1*y
	 * @param G
	 * @param y
	 * @return
	 */
	public Matrix boucleOuverte(Matrix G, Matrix y){
		Matrix Gt  = G.transpose();
		
		Matrix interm = G.times(Gt);
		interm = interm.transpose();
		
		Matrix U = Gt.times(interm).times(y);
		return U;
	}
}
