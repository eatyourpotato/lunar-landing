package com.me.mygdxgame;

/**
 * Énumération permettant de définir les différents états dans lequel le module lunaire peut être.
 */
public enum crashState {
	enVol("En vol"), softLanding("doux"), hardLanding("violent"), crash("fatal");
    private String value;

    private crashState(String value) {
		this.value = value;
	}
    
    public String toString(){
    	return value;
    }
    
    /**
     * Calculer le type d'atterigage en fonction de la vitesse lors de l'impact
     */
    public static String getTypeAtterissage(double vitesseAtterissage){
		if(vitesseAtterissage < 2 ){
			return crashState.softLanding.toString();
		}else if(vitesseAtterissage > 2 && vitesseAtterissage < 15){
			return crashState.hardLanding.toString();
		}else{
			return crashState.crash.toString();
		}
    }
}
