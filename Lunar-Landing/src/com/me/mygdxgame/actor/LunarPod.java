package com.me.mygdxgame.actor;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.me.mygdxgame.Rocket;

/**
 * Classe modélisant le module lunaire d'un point de vue logique et physique
 */
public class LunarPod extends Actor {
	
	private World world;
	private Camera camera;
	private Rocket rok;
	
	// Propriétés du module dans le monde physique
	BodyDef bodyDef;
	Body circleBody;
	CircleShape dynamicCircle;
	FixtureDef fixtureDef;
	PolygonShape shape;
	
	// Gestion du déplacement du module
	public Vector2 modulThrust;
	private int horizontalStep = 80000;
	private int verticalStep = 4000;
	
	private int thrustStep = 4000;
	private int maxThrust = 100000;

	public LunarPod(World world, Camera camera) {
		// Retrieve the world
		this.world = world;
		this.camera = camera;
		
		createLunarPod();
	}
	
	/**
	 * Définir les propriétés et caractéristiques du module
	 */
	private void createLunarPod()	{
		bodyDef = new BodyDef();  
		bodyDef.type = BodyType.DynamicBody;  
		bodyDef.position.set(camera.viewportWidth / 2, camera.viewportHeight * 0.7f);

		circleBody = world.createBody(bodyDef);
		dynamicCircle = new CircleShape();
		dynamicCircle.setRadius(9.1f);
		fixtureDef = new FixtureDef();  
		fixtureDef.shape = dynamicCircle;  
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 0f; 
		fixtureDef.restitution = 0;  
		circleBody.createFixture(fixtureDef);
		circleBody.setUserData(new String("Vaisseau"));

		MassData infoLunarPod = new MassData();
		infoLunarPod.mass = 6839;
		//infoLunarPod.mass = 7500;
		circleBody.setMassData(infoLunarPod);
		
	}
	
	/**
	 * Initialiser les variables du module (par exemple lors d'un repositionnement suite à un reset)
	 */
	public void initLunarPod()	{
		modulThrust = new Vector2(1, -1);

		circleBody.setLinearVelocity(modulThrust);
		Vector2 pos = new Vector2(camera.viewportWidth / 2, 81);
		
		circleBody.setTransform(pos, 0);
		
		// Création de la requette avec les données de depart
		rok = new Rocket(40, pos, new Vector2(0,0));
	}
	
	public void moveLeft()	{
		modulThrust.x = -horizontalStep;
	}
	
	public void moveRight()	{
		modulThrust.x = horizontalStep;
	}
	
	public void moveUp()	{
		if(modulThrust.y < maxThrust)
			modulThrust.y += verticalStep;		
	}
	
	public void moveDown()	{
		if(modulThrust.y > -maxThrust)
			modulThrust.y -= verticalStep;
	}
	
	public void noHorizontalThrust()	{
		modulThrust.x = 0;
	}
	
	public void noVerticalThrust() {
		modulThrust.y = 0;
	}

	public void increaseThrust()	{
		if (modulThrust.y < maxThrust)	{
			modulThrust.y += thrustStep;
		}
	}
	
	public void decreaseThrust()	{
		if (modulThrust.y > 0)	{
			modulThrust.y -= thrustStep;
		}
	}
	
	/**
	 * Easter Egg pour envoyer très haut le module
	 */
	public void jump()	{
		circleBody.applyLinearImpulse(new Vector2(0, 900000), circleBody.getPosition());
	}
	
	/**
	 * Application de la force modulThrust en Newton pour déplacer le module
	 */
	public void move()	{
		circleBody.applyForceToCenter(modulThrust);
	}

	/**
	 * Stoper le déplacement du module dans le monde physique
	 */
	public void stop() {
		circleBody.setLinearVelocity(0, 0);
	}
	
	public Vector2 getPosition()	{
		return circleBody.getPosition();
	}
	
	/**
	 * Obtenir la vitesse de déplacement dans le monde physique
	 * @return x = vitesse horizontale, y = vitesse verticale
	 */
	public Vector2 getVitesse()		{
		return circleBody.getLinearVelocity();
	}
	
	/**
	 * Obtenir la vitesse horizontale et vertical conjuguées
	 * @return
	 */
	public double getVitesseTot() {
		Vector2 vit = getVitesse();
		return Math.sqrt(vit.x*vit.x + vit.y*vit.y);
	}
	
	public Body getBody(){
		return circleBody;
	}
	
	public BodyDef getBodydef(){
		return bodyDef;
	}
	
	/**
	 * MOdification de la position de l'objet
	 */
	public void setPosition(float x, float y){
		circleBody.setTransform(new Vector2(x, y), 0);
		rok.setPosition(new Vector2(x, y));
	}
	
	public Rocket getRocket(){
		return rok;
	}
}
