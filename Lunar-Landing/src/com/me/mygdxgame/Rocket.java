package com.me.mygdxgame;

import Jama.Matrix;

import com.badlogic.gdx.math.Vector2;

/**
 * Classe modélisant le fonctionnement des réacteurs lors du mode physique calculé
 */
public class Rocket {
	/**
	 * Constantes
	 */
	private static final int MASSE_VIDE_MODULE = 6839;
	private static final int MASSE_FUEL_INIT = 8165/10;
	private static final int VITESSE_EJECTION = 4500;
	public static final float G_LUNE = 1.6f;
	public static final int COS_THETA = 1;
	public static final int SIN_THETA = 0;
	public static final int PIX_PAR_METRE = 7;
	
	/**
	 * Variables d'état de la fusée
	 */
	// Position
	private Vector2 pos;
	
	// Vitesse
	private Vector2 vit;
	
	// Acceleration
	private Vector2 acc;
	
	private int masseVide;
	private int masseCourrante;
	private int altitude;
	private int FuelRestant;
	private crashState crashS;
	private int landingSpeed;
	private int tempsVol;
	public int maxThrust;
	public boolean thrustON;
	
	// Coeficient de poussé des réacteurs
	private float erg;
	
	// Commandes
	public Vector2 commande;
	public Vector2 ref;
	public Vector2 consigne;
	
	// Période d'echantillonage
	private float Te;
	
	// Temps depuis le demarrage de la fusée
	private int t;
	
	/**
	 * Constructeur normal
	 * @param f
	 * @param pos
	 * @param vit
	 */
	public Rocket(float f, Vector2 pos, Vector2 vit) {
		this.Te = 25;
		this.pos = pos;
		this.vit = vit;
		this.acc = new Vector2();
		maxThrust = 20;
		commande = new Vector2();
		consigne = new Vector2();
		ref = new Vector2();
		thrustON = false;
		masseVide = 6839;
		FuelRestant = 1000;
		initialize();
		init();
	}
	
	/**
	 * Initialisation de toutes les variables de la fusée
	 */
	public void initialize(){
		masseCourrante = masseVide + MASSE_FUEL_INIT;
		
		// Calcul du coeficient de poussé des reacteurs
		erg = 0.58f;
	}
	
	public float getErg() {
		return erg;
	}

	/**
	 * Modification de la position courante de la fusée
	 * @param pos
	 */
	public void setPosition(Vector2 pos){
		this.pos = pos;
	}
	
	public Vector2 getAcc() {
		return acc;
	}

	/**
	 * Modification de la vitesse de la fusée
	 * @param vit
	 */
	public void setVitesse(Vector2 vit){
		this.vit = vit;
	}
	
	/**
	 * Modification de l'acceleration de la fusée
	 * @param acc
	 */
	public void setAcceleration(Vector2 acc){
		this.acc = acc;
	}
	
	/**
	 * Repositionnement de la fusée pour la remettre à zero
	 * @param posx
	 * @param posy
	 * @param vitx
	 * @param vity
	 */
	public void reInitialize(int posx, int posy, int vitx, int vity){
		this.pos.x = posx;
		this.pos.y = posy;
		this.vit.x = vitx;
		this.vit.y = vity;
		this.acc.x = 0;
		this.acc.y = 0;
	}
	
	public void init(){
		pos = new Vector2(200, 51+30);
		vit = new Vector2(1, -1);
		commande = new Vector2();
		ref = new Vector2();
		FuelRestant = 1000;
	}
	
	/**
	 * Calculer le prochain état de la fusée sans matrice
	 */
	public void nextStepRocket(){
		consigne = ref;
		
		float dt = this.Te / 1000;
		
		// verifier que la fusée est toujours dans la fenetre
		if(pos.y <= 30){
			crashS = crashState.enVol;
			landingSpeed = -1;
			tempsVol += dt;
			return;
		}
		
		/**
		 *  Calcul des nouvelles coordonnées de la fusée
		 */
		//Sauvegardes des valeurs de la fusée dans des valeurs temporaires
		//
		Vector2 p = this.pos;
		Vector2 v = this.vit;
		Vector2 a = this.acc;
		Vector2 ta = this.acc;
		
		// Resultat de la commande
		//
		Vector2 com = this.commande;
		
		// Calcul de la nouvelle accélération
		//
		ta.y = erg*com.y - G_LUNE;
		ta.x = erg*com.x;

		// Calcul de la nouvelle position
		//
		p.x = (float) (p.x + dt * v.x + (0.5*dt*dt*ta.x));
		p.y = (float) (p.y + dt * v.y + (0.5*dt*dt*ta.y));

		// Calcul de la nouvelle vitesse
		//
		v.x = (float) (v.x + ta.x*dt);
		v.y = (float) (v.y + ta.y*dt);
			
		// Faire baisser la quantité de Fuel
		//
		useFuel(dt);
		
		// Affectation des nouvelles valeurs
		//
		this.pos = p;
		this.vit = v;
		this.acc = a;	
	}
	
	/**
	 * Calculer le prochain état de la fusée avec des matrices
	 */
	public void nextStepRocketMatrice(){
		//consigne = ref;
		
		float dt = this.Te / 1000;
		float Te = dt;
		
		// verifier que la fusée est toujours dans la fenetre
		if(pos.y <= 30){
			crashS = crashState.enVol;
			landingSpeed = -1;
			tempsVol += dt;
			return;
		}
		
		/**
		 * Calcul de la matrice A
		 */
		double[][] dA = { 
				{ 1, Te, 0, 0 }, 
				{ 0, 1, 0, 0 }, 
				{ 0, 0, 1, Te },
				{ 0, 0, 0, 1 } 
		};
		
		Matrix A = new Matrix(dA);
		
		/**
		 * Calcul de la matrice Xn
		 */
		double[][] dXn = {
				{pos.x},
				{vit.x},
				{pos.y},
				{vit.y},
		};
		
		Matrix Xn = new Matrix(dXn);
		
		/**
		 * Calcul de la matrice b
		 */
		double [][] db = {
				{erg*Te*Te/2, 	0},
				{erg*Te, 		0},
				{0,		erg*Te*Te/2},
				{0, 		 erg*Te}	
		};
		
		Matrix B = new Matrix(db);
		
		/**
		 * Calcul de la matrice Un
		 */
		double [][] dUn = {
				{commande.x},
				{commande.y - G_LUNE/erg}
		};
		
		Matrix U = new Matrix(dUn);
		
		Matrix Xn1 = A.times(Xn).plus(B.times(U));
		
		double[][] dXn1 = Xn1.getArray();
		
		this.pos = new Vector2((float)dXn1[0][0], (float)dXn1[2][0]);
		this.vit = new Vector2((float)dXn1[1][0], (float)dXn1[3][0]);
	}
	
	/**
	 * Modification de la commande
	 * @param x
	 * @param y
	 */
	public void setCommande(int x, int y){
		this.commande.x = x;
		this.commande.y = y;
	}
	
	/**
	 * Methodes definissant l'utilisation du carburant
	 * @param nombre
	 */
	public void useFuel(float nombre){
		this.FuelRestant -= (Math.abs(this.commande.x)+Math.abs(this.commande.y))*nombre;
		if(this.FuelRestant < 0) this.FuelRestant = 0;
	}

	public Vector2 getPos() {
		return pos;
	}

	public Vector2 getVit() {
		return vit;
	}
	
	/**
	 * Obtenir la vitesse horizontale et verticale conjuguées
	 * @return
	 */
	public double getVitesseTot() {
		Vector2 vit = getVit();
		return Math.sqrt(vit.x*vit.x + vit.y*vit.y);
	}
	
	/**
	 * Calculer la vitesse d'atterisage de la fusée
	 * @return
	 */
	public double landingSpeed(){
		return Math.pow (vit.x*vit.y+vit.y*vit.y, 0.5);
	}
	
	/**
	 * Appliquer une force au module lunaire
	 * @param com
	 */
	public void setThrust(Vector2 com){
		Vector2 t = com;
		Vector2 at = new Vector2(Math.abs(com.x), Math.abs(com.y));
		
		if(at.y > maxThrust){
			com.y = (com.y>0) ? maxThrust : -maxThrust;
			at.y = maxThrust;
		}
		
		if (at.x > maxThrust) {
			com.x = (com.x>0) ? maxThrust : -maxThrust;
			at.x = maxThrust;
		}
		
		t.y = Math.max(at.y, at.x);
		commande = com;
	}
	
	/**
	 * Retourner le fuel restant
	 * @return
	 */
	public int getFuel(){
		return this.FuelRestant;
	}
}
