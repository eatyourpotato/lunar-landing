package com.me.mygdxgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.me.mygdxgame.LunarGame;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

/**
 * Classe permettant de gérer l'écran principal du jeu, permettant de choisir entre plusieurs modes de jeu
 */
public class MainMenuScreen implements Screen {

	private LunarGame myGame;
	private MainMenuScreen hook;

	private Stage stage;
	private TextButton startGameButton;
	private TextButton optionsButton;
	private TextButton exitButton;

	public MainMenuScreen(LunarGame lunarGame) {
		this.myGame = lunarGame;
		this.hook = this;
	}

	/**
	 * Mise en place des composants pour l'affichage du menu
	 */
	@Override
	public void show() {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		Table table = new Table();
		
		Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		startGameButton = new TextButton("Lunar lander", skin);
		startGameButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				myGame.setScreen(new LunarScreen(myGame, hook));
				
				// Prevent listener to capture click during game
				Gdx.input.setInputProcessor(null);
				return true;
			}
		});
	
		optionsButton = new TextButton("Poursuite", skin);
		optionsButton.addListener(new InputListener() {
	
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// Prevent listener to capture click during game
				Gdx.input.setInputProcessor(null);
				myGame.setScreen(new AsteroidGame(myGame, hook));
				//myGame.setScreen(new OptionScreen(myGame, hook));
				return true;
			}
	
		});
		
		exitButton = new TextButton("Exit", skin);
		exitButton.addListener(new InputListener() {
	
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Gdx.app.exit();
				return true;
			}
	
		});
		
		table.setFillParent(true);
		table.add(startGameButton).width(150).height(50);
		table.row();
		table.add(optionsButton).width(150).height(50).padTop(10);
		table.row();
		table.add(exitButton).width(150).height(50).padTop(10);
	
		stage.addActor(table);
	
	}

	/**
	 * Afficher les menu à chaque refraichissement et gérer les entrées
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);  

		stage.act(delta);
		stage.draw();
		
		if (Gdx.input.isKeyPressed(Keys.BACK))	{
			Gdx.app.exit();
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
