package com.me.mygdxgame.screen;

import java.util.ArrayList;

import Jama.Matrix;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.me.mygdxgame.LunarGame;
import com.me.mygdxgame.Releves;

/**
 * Classe modélisant les intéractions pour l'estimation de position
 */
public class AsteroidGame extends LunarScreen {
	
	// Delai entre chaque relevé en seconde
	public static final int DELAI_RELEVE = 1000;
	public static final int NB_RELEVE = 15;
	public static final int TE = 40;
	
	private Body asteroidBody;
	private Vector2 initialPosition;
	private ArrayList<Releves> relevesAngles;
	private int nbReleve;
	private int dt;
	private boolean calculDone;
	private Matrix calculs;
	double timeDeb;
	float Te;
	
	private ArrayList<Vector2> tracePosition;
	private ShapeRenderer afficheurTrace;
	private String messagepose;
	
	private boolean initialisationPossible;
	double tempsPrecedent;
	
	/**
	 * Constructeur normal, permet de mettre en place la simulation
	 * @param myGame	référence vers le jeu
	 * @param hook		référence pour retourner au menu principal
	 */
	public AsteroidGame(LunarGame myGame, MainMenuScreen hook) {
		super(myGame, hook);
		
		relevesAngles = new ArrayList<Releves>();
		initialPosition = new Vector2(100, 150);
		initialisationPossible = false;
		nbReleve = 0;
		calculDone = false;
		timeDeb = System.currentTimeMillis();
		new SpriteBatch();
		messagepose= "";
		
		afficheurTrace = new ShapeRenderer();
		tracePosition = new ArrayList<Vector2>();
	}
	
	/**
	 * Création des paramètres pour la gestion de l'astéroide
	 */
	@Override
	public void show() {
		super.show();
		
		// Create asteroid
		BodyDef bodyDef = new BodyDef();  
		bodyDef.type = BodyType.KinematicBody;  
		bodyDef.position.set(2, camera.viewportHeight * 0.7f);

		asteroidBody = world.createBody(bodyDef);  
		CircleShape dynamicCircle = new CircleShape();  
		dynamicCircle.setRadius(10f);  
		FixtureDef fixtureDef = new FixtureDef();  
		fixtureDef.shape = dynamicCircle;  
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 1f;  
		fixtureDef.restitution = 0;  
		asteroidBody.createFixture(fixtureDef);
		initialisationPossible = true;
		
		initialise();
	}
	
	/**
	 * Remettre à zéro les paramètres de simulation
	 */
	@Override
	public void initialise()	{
		world.setGravity(new Vector2());
		ship.getBody().setLinearVelocity(-15.0f, 0f);	
		
		if (initialisationPossible) {
			asteroidBody.setTransform(initialPosition, 0);
			asteroidBody.setLinearVelocity(10.0f, 7.0f);
			super.initialise();
			relevesAngles = new ArrayList<Releves>();
			tracePosition.clear();
		}
	};
	
	/**
	 * Fonction appelée en boucle pour calculer les changements du monde et afficher à l'écran
	 */
	@Override
	public void render(float delta) {
		super.render(delta);
		
		
		Te = delta;
		
		Vector2 posAstero = asteroidBody.getPosition();
		Vector2 posShip = ship.getPosition();
		
		// Sauvegarder la position précédente.
		tracePosition.add(posShip.cpy());
		
		// Afficher les anciennes positions en trance
		afficheurTrace.begin(ShapeType.Line);
		afficheurTrace.setColor(Color.ORANGE);
		for (int i = 0; i < tracePosition.size() - 1; i++)	{
			afficheurTrace.circle(tracePosition.get(i).x, tracePosition.get(i).y, 1);
		}
		afficheurTrace.end();
		
		/**
		 * Le relevé de l'angle se fait toutes les 45 ms
		 */
		boolean rel = false;
		
		if(tempsEcouleMilli() - tempsPrecedent > DELAI_RELEVE){
			tempsPrecedent = tempsEcouleMilli();
			rel = true;
			nbReleve++;
		}
		
		dt++;
		
		double angle = 0;
		angle = Math.atan2(posAstero.y - posShip.y, posAstero.x - posShip.x);
		Releves releve;
		
		if(rel && nbReleve<NB_RELEVE) {
			releve = new Releves(angle, posShip.x, posShip.y, nbReleve);
			relevesAngles.add(releve);
		}
		
		if(posShip.x < posAstero.x)
			ship.getBody().applyForceToCenter(1000f, 50000f);
		
		if(posShip.y > posAstero.y)
			ship.getBody().applyForceToCenter(100000f, -80000f);
		
		// Affichage des messages 
		String message = "Relevé : " + relevesAngles.size();
		afficherMessage(message, 20, 130);
		
		String message2 = "Angle : " + affCorr(toDegre(angle)) + " degres";
		afficherMessage(message2, 20, 150);
		
		afficherMessage(messagepose, 200, 150);

		/**
		 * Calculer les gradient conjugués si le nombre de releve est satisfaisant
		 */
		if(nbReleve > NB_RELEVE && !calculDone){
			ship.getBody().setLinearVelocity(0, 0);
			calculs = calculGradient();
		}
		
		// Faire atterir l'observateur sur le mobile
		if(nbReleve>NB_RELEVE){
			Vector2 pos = calculerPosition(calculs);
			
			mode = 1;
			touchedScreenPosition.x = pos.x;
			touchedScreenPosition.y = pos.y;
			UpdatePhys(delta);
		}
		
	}

	/**
	 * Calcul de la position du mobile à partir des relevés
	 * @return
	 */
	public Matrix calculGradient(){
		calculDone = true;
		Matrix retour = null;
		
		double tabC[] = new double[4];
		
		/**
		 * Matrix tanporaire db
		 */
		double[][] db = {
				{0},
				{0},
				{0},
				{0}};
		Matrix b = new Matrix(db);
		
		/**
		 * Matrice temporaire theta
		 */
		double[][] dtheta = {{0,0,0,0,},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
		Matrix theta = new Matrix(dtheta);
		
		/**
		 * Faire le calcul pour l'ensemble des relevés
		 */
		for (Releves rel : relevesAngles) {
			double angle = rel.angle;
			float instantT = rel.t;
			Vector2 posObs = rel.position;

			/**
			 * Calcul des Ci à l'instant T
			 */
			double[][] dCi = {
					{Math.sin(angle)},
					{Math.sin(angle)*instantT},
					{-Math.cos(angle)},
					{-Math.cos(angle)*instantT}};
			
			tabC[0] = dCi[0][0];
			tabC[1] = dCi[1][0];
			tabC[2] = dCi[2][0];
			tabC[3] = dCi[3][0];
			
			/**
			 * Calcul de la matrix theta temporaire
			 */
			double[][] dthetatmp = {
					{tabC[0]*tabC[0], tabC[0]*tabC[1], tabC[0]*tabC[2], tabC[0]*tabC[3]},
					{tabC[1]*tabC[0], tabC[1]*tabC[1], tabC[1]*tabC[2], tabC[1]*tabC[3]},
					{tabC[2]*tabC[0], tabC[2]*tabC[1], tabC[2]*tabC[2], tabC[2]*tabC[3]},
					{tabC[3]*tabC[0], tabC[3]*tabC[1], tabC[3]*tabC[2], tabC[3]*tabC[3]}};
			Matrix thetatmp = new Matrix(dthetatmp);
			theta = theta.plus(thetatmp);
			
			/**
			 * Calcul de la matrice b
			 */
			
			double taby = Math.cos(angle)*posObs.y - Math.sin(angle)*posObs.x;
			
			double[][] dbtemp = {	
					{tabC[0]*taby},
					{tabC[1]*taby},
					{tabC[2]*taby},
					{tabC[3]*taby}};
			
			Matrix Mbtemp = new Matrix(dbtemp);
			b = b.plus(Mbtemp);
		}
		
		/**
		 * Utilisation de la méthode solve de la librairie
		 * Jama
		 */
		retour = theta.solve(b).times(-1);
		
		System.out.println("calcul fait : x0 = " + retour.getArray()[0][0] + " y0 = " + retour.getArray()[2][0]);
		System.out.println("calcul fait : vx = " + retour.getArray()[1][0] + " vy = " + retour.getArray()[3][0]);
		
		return retour;
	}
	
	/**
	 * Calcul de la position de l'object en fonction de sa position initiale et de sa vitesse
	 * @param def
	 */
	public Vector2 calculerPosition(Matrix def){

		float posx = (float) (getPosition(def).x+getVitesse(def).x*Te*dt);
		float posy = (float) (getPosition(def).y+getVitesse(def).y*Te*dt);
		
		return new Vector2(posx, posy);
	}
	
	/**
	 * REtourne la position contenue dans la matrice M
	 * La position x se trouve à la position 0 et y position 2
	 * @param m
	 */
	public Vector2 getPosition(Matrix m){
		double[][] d = m.getArray();
		return new Vector2((float)d[0][0], (float)d[2][0]);
	}
	
	/**
	 * Retourne la vitesse contenue dans la matrice M
	 * La position x se trouve a la position 1 et y position 3
	 * @param m
	 */
	public Vector2 getVitesse(Matrix m){
		double[][] d = m.getArray();
		return new Vector2((float)d[1][0], (float)d[3][0]);
	}
	
	/**
	 * Méthode de test du gradient
	 */
	public void testGradient(){
		calculDone = true;
		double[][] dth = {{2, -3}, {1,5}};
		double[][] db = {{4}, {15}};
		
		Matrix th =new Matrix(dth);
		Matrix b = new Matrix(db);
		
		Matrix x = new Matrix(2, 1);
		
		Matrix ret = gradientConjugue(th, x, b);
		System.out.println("x = " + ret.getArray()[0][0]);
		System.out.println("y = " + ret.getArray()[1][0]);
		
		Matrix ret2 = th.solve(b);
		System.out.println("x = " + ret2.getArray()[0][0]);
		System.out.println("y = " + ret2.getArray()[1][0]);
	}

	/**
	 * Conversion de radian en degre
	 * @param radian
	 * @return
	 */
	public double toDegre(double radian){
		return 180*radian/Math.PI+180;
	}
	
	/**
	 * Converstino de degre en radiant
	 * @param degre
	 * @return
	 */
	public double toRadian(double degre){
		return Math.PI*degre/180 - 180;
	}
	
	/**
	 * Calculer le gradient conjugué pour trouver le resultat
	 * sous la forme Ax = b
	 * @param theta
	 * @param x
	 * @param b
	 * @return
	 */
	public Matrix gradientConjugue(Matrix theta, Matrix x, Matrix b){
		int width = x.getColumnDimension();
		int height = x.getRowDimension();
		
		int L = theta.getRowDimension();
		
		// Initialisation de X0
		double[][] dXO = new double[height][width];
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++)
				dXO[i][j] = 0;
		
		// Création de la matrice x0 correspondante
		Matrix xO = new Matrix(dXO);
		
		// Création de la matrice g0
		Matrix gO = theta.times(xO);
		gO = gO.minus(b);
		
		// Affectation de h0
		Matrix hO = gO;
		
		// Création d'un tableau pour le sauvegarde des valeurs
		Matrix[] tabX = new Matrix[L];
		Matrix[] tabG = new Matrix[L];
		Matrix[] tabH = new Matrix[L];
		tabX[0] = xO;
		tabG[0] = gO;
		tabH[0] = hO;
				
		// Iteration
		for (int n=1; n<L; n++){
			
			/**
			 *  Calcul de xn
			 */
			Matrix denominateur = tabH[n-1].transpose();
			denominateur = denominateur.times(theta);
			denominateur = denominateur.times(tabH[n-1]);
			denominateur = denominateur.inverse();
			
			Matrix numerateur = tabH[n-1].transpose();
			numerateur = numerateur.times(tabG[n-1]);
			
			Matrix fraction = numerateur.times(denominateur);
			Matrix champsDroit = tabH[n-1].times(fraction);

			tabX[n] = tabX[n-1].minus(champsDroit);
			
			/**
			 * Calcul de gn
			 * le champs droit ne change pas.
			 */
			Matrix champsDroitG = theta.times(champsDroit);
			tabG[n] = tabG[n-1].minus(champsDroitG);
			
			/**
			 * Calcul de hn
			 * Le denominateur ne change pas.
			 */
			Matrix numerateurH = tabH[n-1].transpose();
			numerateurH = numerateurH.times(theta);
			numerateurH = numerateurH.times(tabG[n]);
					
			Matrix membreDroit = tabH[n-1].times(numerateurH);
			membreDroit = membreDroit.times(denominateur);
			
			tabH[n] = tabG[n].minus(membreDroit);
		}
		
		return tabX[L-1];
	}
}
