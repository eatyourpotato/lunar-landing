package com.me.mygdxgame.screen;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Jama.Matrix;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.me.mygdxgame.CollisionListener;
import com.me.mygdxgame.EnumModeStat;
import com.me.mygdxgame.LunarGame;
import com.me.mygdxgame.Utilities;
import com.me.mygdxgame.crashState;
import com.me.mygdxgame.actor.LunarPod;

/**
 * Classe modélisant le déplacement du module lunaire dans l'espace
 */
public class LunarScreen implements Screen {

	private LunarGame myGame;
	private MainMenuScreen hook;
	private SpriteBatch batch;
	BitmapFont fontmessage ;
	
	/**
	 * Differents modes de jeux
	 * mode 0 => Mode normal avec la physique généré
	 * mode 1 => Mode avec retour d'etat
	 */
	protected int mode = 0;
	private float deltaRef = 1;
	public String messageChoc;
	public boolean contactMessageDisplayed;
	private boolean carb = true;
	
	
	World world;
	Box2DDebugRenderer debugRenderer;  
	OrthographicCamera camera;
	
	// Paramètres pour gérer la relation entre la vitesse de rendu physique et la vitesse d'affichage
	static final float BOX_STEP=1/40f;
	static final int BOX_VELOCITY_ITERATIONS=8;  
	static final int BOX_POSITION_ITERATIONS=3;  
	static final float WORLD_TO_BOX=0.01f;  
	static final float BOX_WORLD_TO=100f; 
	static final float GRAVITY_WORLD=1.6f;
	
	float accumulator;
	float nonPhysAccumulator;
	private long time;
	private long timerReset;
	private long frameTime;
	private FPSLogger fpsLogger;
	
	Body groundBody;

	protected boolean touchedGround;
	protected boolean touchedGroundDone;
	
	protected LunarPod ship;
	
	protected Vector2 touchedScreenPosition;

	// Paramètres d'affichage de la destination
	private Boolean drawCrosshair;
	private Pixmap crosshair_Bitmap;
	private Texture crosshair_Texture;
	
	/**
	 * Listes pouvant contenir les commandes
	 */
	int n;
	ArrayList<Float> com;
	ArrayList<Float> K;

	public void setTouchedGround(boolean flag)	{
		touchedGround = flag;
	}
	
	/**
	 * Constructeur normal mettant en place la simulation
	 */
	public LunarScreen(LunarGame myGame, MainMenuScreen hook) {
		this.myGame = myGame;
		this.hook = hook;
		fontmessage = new BitmapFont();
		batch       = new SpriteBatch();
		messageChoc = "";
		n=0;
		
		fpsLogger = new FPSLogger();
	}

	/**
	 * Mise en place des paramètres nécessaires au fonctionnement de la simulation
	 */
	@Override
	public void show() {
		Gdx.input.setCatchBackKey(true);
		
		world = new World(new Vector2(0, -GRAVITY_WORLD), true); 
		world.setContactListener(new CollisionListener(this));

		camera = new OrthographicCamera();
		camera.viewportHeight = 480;  
		camera.viewportWidth = 800;  
		camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
		camera.update();

		//Création du sol  
		BodyDef groundBodyDef =new BodyDef();  
		groundBodyDef.position.set(new Vector2(0, 10f));  
		groundBody = world.createBody(groundBodyDef);  
		PolygonShape groundBox = new PolygonShape();  
		groundBox.setAsBox((camera.viewportWidth) * 2, 10.0f);  
		groundBody.createFixture(groundBox, 0.0f); 
		groundBody.setUserData(new String("Ground"));

		debugRenderer = new Box2DDebugRenderer(); 

		timerReset = 0;
		
		ship = new LunarPod(world, camera);
		
		com = Utilities.getFile("data/com.txt");
		K = Utilities.getFile("data/K.txt");
		
		initialise();
		createCrosshair();
	}
	
	/**
	 * Initialisation des variables de fonctionnement de la simulation
	 */
	public void initialise()	{
		if (System.currentTimeMillis() - timerReset > 1000)	{
		
		touchedGround = false;
		touchedGroundDone = false;
		contactMessageDisplayed = false;
		
		ship.initLunarPod();

		time = System.currentTimeMillis();
		timerReset = time;
		messageChoc = "";
		
		touchedScreenPosition = new Vector2();
		
		drawCrosshair = false;
		
		}
	}
	
	/**
	 * Permet de créer une croix pour marquer la destination demandée pour l'atterrissage
	 */
	private void createCrosshair()	{
		// Create an empty dynamic pixmap
		 crosshair_Bitmap = new Pixmap(64, 64, Pixmap.Format.RGBA8888);
		 crosshair_Bitmap.setColor(150, 150, 150, 1);

		// upper left corner
		 crosshair_Bitmap.drawLine(0, 0, 0, 10);
		 crosshair_Bitmap.drawLine(0, 0, 10, 0);
		 crosshair_Bitmap.drawLine(1, 1, 1, 10);
		 crosshair_Bitmap.drawLine(1, 1, 10, 1);
		 

		// upper right corner
		 crosshair_Bitmap.drawLine(63, 0, 63, 10);
		 crosshair_Bitmap.drawLine(63, 0, 53, 0);
		 crosshair_Bitmap.drawLine(62, 1, 62, 10);
		 crosshair_Bitmap.drawLine(62, 1, 53, 1);
		 

		// lower right corner
		 crosshair_Bitmap.drawLine(63, 63, 63, 53);
		 crosshair_Bitmap.drawLine(63, 63, 53, 63);
		 crosshair_Bitmap.drawLine(62, 62, 62, 53);
		 crosshair_Bitmap.drawLine(62, 62, 53, 62);
		 

		// lower left corner
		 crosshair_Bitmap.drawLine(0, 63, 0, 53);
		 crosshair_Bitmap.drawLine(0, 63, 10, 63);
		 crosshair_Bitmap.drawLine(1, 62, 1, 53);
		 crosshair_Bitmap.drawLine(1, 62, 10, 62);
		 

		// Create a texture to contain the pixmap
		 crosshair_Texture = new Texture(crosshair_Bitmap);

		// Blit the composited overlay to a texture
		 crosshair_Texture.draw(crosshair_Bitmap, 0, 0);
		 crosshair_Bitmap.dispose();
	}
	
	/**
	 * Méthode render executer à chaque itération de calcul pour calculer l'avancement dans la simulation
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);  
		debugRenderer.render(world, camera.combined);  
		
		/**
		 * Gestion du carburant
		 */
		if(mode == 0)
			delta = (float) 0.00001;
		
		if (!gererCarburant(delta)) {
			messageChoc = "Plus de carburant !!!";
			afficherLesMessages();
			gererEntreesSansMoteur();
			gererEntrees();
			return;
		}
		
		// Gestion des raccourcis
		gererRaccourcis();
		
		/**
		 * Recuperation du mode de jeu
		 */
		getMode();
		
		/**
		 * Selon le mode de jeu
		 */
		switch (mode) {
		
		/**
		 * Mode normal avec moteur physique 'n'
		 */
		case 0:
			UpdatePhys(Gdx.graphics.getDeltaTime());
			ship.getRocket().getPos().x = ship.getPosition().x;
			ship.getRocket().getPos().y = ship.getPosition().y;
			
			ship.getRocket().commande.x = ship.modulThrust.x;
			ship.getRocket().commande.y = ship.modulThrust.y;
			
			gererEntrees();
			break;

		/**
		 * Mode avec retour d'etat 'e'
		 */
		case 1:
			afficherCrosshair();
			gererContactSolSansMoteur();
			
			Vector2 pos = ship.getRocket().getPos();
			Vector2 vit = ship.getRocket().getVit();
			
			// Initialiser le point de position par défaut au sol
			Vector2 cons = new Vector2();
			
			if (touchedScreenPosition.equals(cons))	{
				cons.x = 220;
				cons.y = 37f;
				// Correspond en position croix à (335, 0)
			}
			else	{
				cons = new Vector2(touchedScreenPosition.x, touchedScreenPosition.y);
			}
			
			ship.getRocket().commande = calculerCommandeRetourEtat(cons, pos, vit);
			
			UpdatePosition(Gdx.graphics.getDeltaTime());
			break;
			
		/**
		 * Mode normal sans moteur physique 'm'
		 */
		case 2 :
			gererContactSolSansMoteur();
			
			// Calcul de la commande a partir des entrés clavier
			ship.getRocket().commande.x = ship.getRocket().ref.x;
			ship.getRocket().commande.y = ship.getRocket().ref.y;

			UpdatePosition(Gdx.graphics.getDeltaTime());
			
			gererEntreesSansMoteur();
			break;
			
		/**
		 * Commande en boucle ouverte à horizon fini 'o'
		 */
		case 3:
			int m = 2 * n;

			if (m < com.size()) {
				n++;
				Vector2 commande = new Vector2();
				commande.x = com.get(m);
				commande.y = com.get(m + 1) - ship.getRocket().G_LUNE
						/ ship.getRocket().getErg();
				
				ship.getRocket().commande = commande;
			}else{
				
				ship.getRocket().commande = new Vector2(0, 0);
			}
			
			UpdatePosition(Gdx.graphics.getDeltaTime());
			gererEntreesSansMoteur();
			break;
		
		
		/**
		 * Commande en vitesse 'v'
		 */
		case 4:
			afficherCrosshair();
			gererContactSolSansMoteur();
			
			Vector2 po = ship.getRocket().getPos();
			Vector2 vi = ship.getRocket().getVit();
			
			// Initialiser le point de position par défaut au sol
			Vector2 con = new Vector2();
			
			if (touchedScreenPosition.equals(con))	{
				con.x = 220;
				con.y = 37f;
				// Correspond en position croix à (335, 0)
			}
			else	{
				cons = new Vector2(touchedScreenPosition.x, touchedScreenPosition.y);
			}
			
			ship.getRocket().commande = calculCommandeVitesse(con, vi);
			
			UpdatePosition(Gdx.graphics.getDeltaTime());
			break;
			
			/**
			 * Commande optimale a horizon fini 'h'
			 */
		case 5:
			m = 8*n;
			
			Vector2 commande = new Vector2();
			if(m < K.size()){
				n++;
				Vector2 position = ship.getRocket().getPos();
				Vector2 vitesse = ship.getRocket().getVit();
				Vector2 consigne = ship.getRocket().getVit();
				
				
				commande.x = K.get(m)*(consigne.x-position.x)-K.get(m+1)*vitesse.x+K.get(m+2)*(consigne.y-position.y)-K.get(m+3)*vitesse.y;
				commande.y = K.get(m+4)*(consigne.x-position.x)-K.get(m+5)*vitesse.x+K.get(m+6)*(consigne.y-position.y)-K.get(m+7)*vitesse.y;
				commande.y += ship.getRocket().G_LUNE/ship.getRocket().getErg();
			}else{
				commande = new Vector2(0,0);
			}
			
			ship.getRocket().commande = commande;
			UpdatePosition(Gdx.graphics.getDeltaTime());
			gererEntreesSansMoteur();
			break;
		}
		
		gererContactSol();
		afficherLesMessages();
	}
	
	/**
	 * Méthode de gestion du contact avec le sol pour arrêter la simulation
	 */
	private void gererContactSol() {
		if (touchedGround && !touchedGroundDone)	{
			System.out.println("Temps écoulé : " + tempsEcouleSecond() + " s");
			ship.stop();
			touchedGroundDone = true;
			messageChoc += " in " + affCorr(tempsEcouleSecond()) + " s";
			Gdx.input.vibrate(100);
		}
	}
	
	/**
	 * Gestion du fuel
	 */
	private boolean gererCarburant(float delta){
		ship.getRocket().useFuel(delta);
		
		if(ship.getRocket().getFuel() <= 0)
			return false;
		
		return true;
	}
	
	/**
	 * Calculer la commande à appliquer pour une consigne donnée.
	 * @param cons
	 * @param pos
	 * @param vit
	 * @return
	 */
	private Vector2 calculerCommandeRetourEtat(Vector2 cons, Vector2 pos, Vector2 vit){
		double[][] dK = {{0.33, 1.11, 0.0, 0.0}, {0.33, 0.0, 0.5, 1.11}};
		double[][] consigne = {{cons.x}, {0.}, {cons.y},{ 0.}};
		double[][] position = {{pos.x}, {vit.x}, {pos.y}, {vit.y}};
		
		Matrix K = new Matrix(dK);
		Matrix Cn = new Matrix(consigne);
		
		Matrix Xn = new Matrix(position);
		Matrix erreurCommande = Cn.minus(Xn);
		
		Matrix comm = K.times(erreurCommande);
		double[][] result = comm.getArray();
		return new Vector2((float)result[0][0], (float)result[1][0]);
	}
	
	/**
	 * Calcul de la commande en vitesse
	 * @param cons
	 * @param vit
	 * @return
	 */
	private Vector2 calculCommandeVitesse(Vector2 cons, Vector2 vit){
		double[][] dK = {{42.5306, 42.5306}};
		Matrix K = new Matrix(dK);
		
		double[][] dconsigne = {{cons.x}, {0.}, {cons.y},{ 0.}};
		Matrix consigne = new Matrix(dconsigne);
		
		double[][] dvitesse = {{vit.x}, {0}, {vit.y}, {0}};
		Matrix vitesse = new Matrix(dvitesse);
		
		Matrix result = K.times(consigne.minus(vitesse));
		double[][] dresult = result.getArray();
		
		return new Vector2((float)dresult[0][0],(float) dresult[0][1]);
	}

	/**
	 * Méthode de verification du contact avec le sol sans moteur physique
	 */
	private void gererContactSolSansMoteur(){
		// Verifier qu'il n'y ai pas contact avec le sol
		if(ship.getRocket().getPos().y <= 30){
			if (!contactMessageDisplayed){
				this.messageChoc = "Atterissage "+ crashState.getTypeAtterissage(ship.getRocket().getVitesseTot()) +": "; 
			    this.messageChoc += this.affCorr(ship.getRocket().getVitesseTot(), 2) + " m/s in " + affCorr(tempsEcouleSecond()) + " s";	
				contactMessageDisplayed = true;
			}
		}
	}
	
	/**
	 * Methode d'affichage des messages
	 */
	public void afficherLesMessages(){
		String message4 = "Mode : " + EnumModeStat.getEnum(mode);
		String message = "Altitude : " + affCorr(ship.getPosition().y-30+0.9f) + " m";
		String message2 = "Vitesse : ";
		if(mode == 2)
			message2 += affCorr(ship.getRocket().getVitesseTot()) + " m/s";
		else
			message2 += affCorr(ship.getVitesseTot()) + " m/s";
		
		String message6 = "Fuel : " + affCorr(ship.getRocket().getFuel());
		String message3 = "Temps écoulé : " + affCorr(tempsEcouleSecond()) + " s";
		
		afficherMessage(message4, 20, 130);
		afficherMessage(message6, 20, 110);
		afficherMessage(message, 20, 90);
		afficherMessage(message2, 20, 70);
		afficherMessage(message3, 20, 50);
		afficherMessage(messageChoc, 300, 100);
	}
	
	/**
	 * Methode permettant d'afficher a l'ecran un viseur là où l'utilisateur a cliqué
	 * 
	 */
	public void afficherCrosshair(){
		
		// Get position on the screen
		if (Gdx.input.justTouched())	{
			
			touchedScreenPosition.x = Gdx.input.getX();
			touchedScreenPosition.x = touchedScreenPosition.x;
			
			touchedScreenPosition.y = Gdx.input.getY();
			touchedScreenPosition.y = 480 - touchedScreenPosition.y;
			
			System.out.println(touchedScreenPosition);
			drawCrosshair = true;
		}
		
		// prevent crosshair to be displayed on start
		if (drawCrosshair && !touchedScreenPosition.equals(new Vector2()))	{
			batch.begin();
			batch.draw(crosshair_Texture, touchedScreenPosition.x - 30, touchedScreenPosition.y - 30);
			batch.end();
		}
	}
	
	/**
	 * Méthode de gestion des raccourcis claviers ingame
	 */
	public void gererRaccourcis()	{
		if(Gdx.input.isKeyPressed(Keys.R))	{
			initialise();
		}
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)  || Gdx.input.isKeyPressed(Keys.BACK))	{
			Gdx.input.setInputProcessor(null);
			myGame.setScreen(hook);
		}
	}

	/**
	 * Méthode de gestion des entrées pour le controle manuel avec le moteur physique
	 */
	public void gererEntrees(){

		if (!touchedGround)	{
			if(Gdx.input.isKeyPressed(Keys.LEFT))	{
				ship.moveLeft();
			}
			else if(Gdx.input.isKeyPressed(Keys.RIGHT))	{
				ship.moveRight();
			}
			else	{
				ship.noHorizontalThrust();
			}
			
			if(Gdx.input.isKeyPressed(Keys.SPACE))	{
				ship.jump();
			}
			
			if(Gdx.input.isKeyPressed(Keys.UP))	{
				//ship.decreaseThrust();
				ship.moveUp();
			} else if(Gdx.input.isKeyPressed(Keys.DOWN))	{
				//ship.increaseThrust();
				ship.moveDown();
			}
			else	{
				ship.noVerticalThrust();
			}
			ship.move();
		}
	}
	
	
	/**
	 * Méthode de gestion des entrées pour le controle manuel sans le moteur physique
	 */
	public void gererEntreesSansMoteur(){
	
		int max = ship.getRocket().maxThrust;
		
		if(Gdx.input.isKeyPressed(Keys.LEFT))	{
			if(ship.getRocket().ref.x > -max)
				ship.getRocket().ref.x -= deltaRef;
			ship.getRocket().thrustON = true;
		}else if(Gdx.input.isKeyPressed(Keys.RIGHT))	{
			if(ship.getRocket().ref.x < max)
				ship.getRocket().ref.x += deltaRef;
			ship.getRocket().thrustON = true;
		}else {
			ship.getRocket().thrustON = false;
			ship.getRocket().ref.x = 0;
		}
			
			
		if(Gdx.input.isKeyPressed(Keys.UP))	{
			if(ship.getRocket().ref.y < max)
				ship.getRocket().ref.y += deltaRef;
			ship.getRocket().thrustON = true;
		}else if(Gdx.input.isKeyPressed(Keys.DOWN))	{
			if(ship.getRocket().ref.y > -max)
				ship.getRocket().ref.y -= deltaRef;
			ship.getRocket().thrustON = true;
		} else{
			ship.getRocket().thrustON = false;
			ship.getRocket().ref.y = 0;
		}
		
		if(Gdx.input.isKeyPressed(Keys.R))	{
			ship.getRocket().init();
			this.messageChoc = "";
			time = System.currentTimeMillis();
		}
		
		ship.getRocket().setThrust(ship.getRocket().commande);
	}

	/**
	 * Methode permettant de connaitre le mode de jeu auquel souhaite jouer le joueur
	 * elle affecte automatiquement la variable globale mode avec les valeurs suivantes
	 * 
	 * Press E => Mode avec retour d'etat
	 * Press N => Mode normal
	 */
	public void getMode(){
		if(Gdx.input.isKeyPressed(Keys.E))	{
			mode = 1;
		}
		
		if(Gdx.input.isKeyPressed(Keys.M))	{
			mode = 2;
		}
		
		if(Gdx.input.isKeyPressed(Keys.O))	{
			mode = 3;
		}
		
		if(Gdx.input.isKeyPressed(Keys.V))	{
			mode = 4;
		}
		
		if(Gdx.input.isKeyPressed(Keys.N))	{
			mode = 0;
		}
		
		if(Gdx.input.isKeyPressed(Keys.H))	{
			mode = 5;
		}
	}
	
	/**
	 * Methode permettant d'afficher à l'ecran un message ayant comme position x et y;
	 * @param message
	 * @param x
	 * @param y
	 */
	public void afficherMessage(String message, float x, float y){
		batch.begin();
		fontmessage.draw(batch, message, x, y);  
		batch.end();
	}
	
	/**
	 * Permet de régler le format d'affichage des doubles avec un chiffre aprés la virgule
	 * @param nb
	 * @param chiffreVirgule indique le nombre de chiffre aprés la virgule
	 * @return
	 */
	public String affCorr(double nb, int chiffreVirgule){
		DecimalFormat df = new DecimalFormat () ;
		
		//arrondi à 2 chiffres apres la virgules
		df.setMaximumFractionDigits (chiffreVirgule);
		df.setDecimalSeparatorAlwaysShown (true);
		return df.format(nb);
	}
	
	/**
	 * Permet de régler le format d'affichage des doubles avec un chiffre aprés la virgule avec le parametre par defaut
	 * @param nb
	 * @return
	 */
	public String affCorr(double nb){
		return affCorr(nb, 1);
	}
	
	
	/**
	 * Méthode permettant de gérer le décalage entre la simulation physique et l'affichage à l'écran avec le moteur physique
	 * @param dt
	 */
	public void UpdatePhys(float dt){
		accumulator+=dt;
		while(accumulator>BOX_STEP){
			world.step(BOX_STEP,BOX_VELOCITY_ITERATIONS,BOX_POSITION_ITERATIONS);
			accumulator-=BOX_STEP;
		}
	}
	
	/**
	 * Méthode permettant de gérer le décalage entre la simulation physique et l'affichage à l'écran sans le moteur physique
	 * @param dt
	 */
	public void UpdatePosition(float dt){
		nonPhysAccumulator+=dt;
		while(nonPhysAccumulator>BOX_STEP){
			ship.getRocket().nextStepRocketMatrice();
			ship.setPosition(ship.getRocket().getPos().x, ship.getRocket().getPos().y);
			nonPhysAccumulator-=BOX_STEP;
		}
	}
	
	@Override
	public void hide() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		world.dispose();
		debugRenderer.dispose();
	}
	
	public double tempsEcouleMilli(){
		return (System.currentTimeMillis() - time);
	}
	
	public double tempsEcouleSecond(){
		return tempsEcouleMilli()/1000;
	}

}
