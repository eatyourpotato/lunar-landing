package com.me.mygdxgame;

/**
 * Énumération permettant de modéliser le mode de rendu et de contrôle du module lunaire.
 */
public enum EnumModeStat {
	NORMAL("sans moteur"), RETOUR_ETAT("retour d'état"), NORMAL_MOTEUR("normal"), BOUCLE_OUVERTE(
			"boucle ouverte"), VITESSE("vitesse"), OPTIMALE(
			"optimale");

	String mode;

	private EnumModeStat(String mode) {
		this.mode = mode;
	}

	public String getValue() {
		return mode;
	}

	public static String getEnum(int num) {
		switch (num) {
		case 0:
			return NORMAL_MOTEUR.getValue();
		case 1:
			return RETOUR_ETAT.getValue();
		case 2:
			return NORMAL.getValue();
		case 3:
			return BOUCLE_OUVERTE.getValue();
		case 4:
			return VITESSE.getValue();
		case 5:
			return OPTIMALE.getValue();
		}
		return "defaut";
	}
}
