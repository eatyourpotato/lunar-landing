package com.me.mygdxgame;

import com.badlogic.gdx.Game;
import com.me.mygdxgame.screen.MainMenuScreen;

/**
 * Point de départ générique du jeu, il est appelé par les différents lanceurs en fonction des plateformes. <br>
 * Lors de son appel, il affiche l'écran principal du jeu.
 */
public class LunarGame extends Game {

	@Override
	public void create() {
		setScreen(new MainMenuScreen(this));
	}

}
