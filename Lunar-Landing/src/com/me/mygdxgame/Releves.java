package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;

/**
 * Classe modélisant un relevé effectué par un observateur lors de l'estimation de paramètre.
 */
public class Releves {
	public double angle;
	public Vector2 position;
	public float t;
	
	public Releves() {
		angle=0f;
		position = new Vector2();
	}
	
	public Releves(double angle, Vector2 position, float t){
		this.angle = angle;
		this.position = position;
		this.t = t;
	}
	
	public Releves(double angle2, float posX, float posY, float t){
		this.angle = angle2;
		this.position = new Vector2(posX, posY);
		this.t = t;
	}
	
	@Override
	public String toString() {
		return "Angle : " + angle + " Position " + position + " t : " + t;
	}
}
