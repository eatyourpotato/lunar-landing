package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

/**
 * Classe permettant de parser les fichiers de commande
 */
public class Utilities {

	public static ArrayList<Float> getFile(String path){
		FileHandle file;
		ArrayList<Float> list = new ArrayList<Float>();

			file = Gdx.files.internal(path);
			String content = file.readString();
			String[] lines = content.split("\n");

			for (String line : lines)	{
				String[] split = line.split(",");

				for (String string : split) {
					list.add(Float.parseFloat(string));
				}
			}

		return list;
	}


	public static void main(String[] args) {
		System.out.println(Utilities.getFile("data/com.txt").get(0));
	}
}
